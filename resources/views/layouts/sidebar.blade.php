@php
    $user = Auth::user();
    if($user->hasRole('root-admin')){
        $icons = App\Models\Menu::orderBy('order_by', 'asc')->where('sidebar', '!=', '0')->get();
    }elseif ($user->hasRole('admin')){
        $icons = App\Models\Menu::orderBy('order_by', 'asc')->where('sidebar', '!=', '0')->whereNotIn('id',['1', '2'])->get();
    }else{
        $icons = App\Models\Menu::orderBy('order_by', 'asc')->where('sidebar', '!=', '0')->whereNotIn('id',['1', '2','3'])->get();
    }
@endphp
@if(config('adminlte.layout') != 'top-nav')
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                @if(isset($icons))
                    @foreach($icons as $icon)
                        <a href="{{ route(strtolower($icon->name).'.index') }}">
                            <li class="header">
                                <i class="{!! $icon->icon !!}">&nbsp;&nbsp;{{ $icon->name }}</i>
                            </li>
                        </a>
                    @endforeach
                @endif
                @yield('sidebar')
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
@endif