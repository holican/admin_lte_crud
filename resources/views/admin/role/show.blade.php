@extends('admin.home')

@section('content_header')

@stop

@section('content')
<div class="panel-body">
    <div class="col-sm-6">
        <table class="table table-striped table-bordered">
            <!-- Id Field -->
            <tr>
                <th>{!! Form::label('id', 'Id:') !!}</th>
                <td>{!! $role->id !!}</td>
            </tr>
            <!-- Name Field -->
            <tr>
                <th>{!! Form::label('name', 'Name:') !!}</th>
                <td>{!! $role->name !!}</td>
            </tr>
        </table>
        <a href="{!! route('roles.index') !!}" class="btn btn-primary">Back</a>
    </div>
</div>
@stop