@extends('admin.home')

@section('content_header')
    <ol class="breadcrumb pull-right">
        <li class="breadcrumb-item"><a class="btn btn-primary" href="{!! route('roles.create') !!}" style="color:white">Add New</a></li>
    </ol>
    <br>
@stop

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block" style="margin-top: 3%;">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    <label for="roles" class="pull-left">&nbsp;Roles</label>
    <table class="table display table-striped table-bordered dt-responsive" >
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($roles as $key=>$role)
                <tr>
                    <td>{!! $role->id !!}</td>
                    <td>{!! $role->name !!}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{!! route('roles.show', [$role->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
                            <a href="{!! route('roles.edit', [$role->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-edit"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop

