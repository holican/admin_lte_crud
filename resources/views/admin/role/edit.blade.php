@extends('admin.home')

@section('content_header')
    <div class="text-center">
        <label for="role">Update Role</label>
    </div>
@stop

@section('content')
<div class="panel-body">
    {!! Form::open(['route' => ['roles.update', $role->id], 'method' => 'put']) !!}
        <div class="form-group col-sm-4">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', $role->name, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-12">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('roles.index') !!}" class="btn btn-default">Cancel</a>
    {!! Form::close() !!}
</div>
@stop