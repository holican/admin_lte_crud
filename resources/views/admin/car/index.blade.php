@extends('admin.home')

@section('css')

@stop

@section('content_header')
    <ol class="breadcrumb pull-right">
        @if(Auth::user()->hasRole('root-admin') || Auth::user()->hasRole('admin'))
            <li class="breadcrumb-item"><a class="btn btn-success" href="{!! route('tables.index', [$table]) !!}" style="color:white">Modify Table Columns</a></li>
        @endif
        <li class="breadcrumb-item"><a class="btn btn-primary" href="{!! route('car.create') !!}" style="color:white">Add New Car</a></li>
    </ol>
    <br><br>
@stop

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block" style="margin-top: 3%;">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('danger'))
        <div class="alert alert-danger alert-block" style="margin-top: 3%;">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="panel panel-inverse" data-sortable-id="index-1">
        <div class="panel-heading">
            <label for="Car" class="pull-left">&nbsp;Car</label>
        </div>
         <div class="panel-body">
            <table class="table display table-striped table-bordered dt-responsive" >
                <thead>
                    <tr>
                        @foreach($columns as $column)
                            <th>{!! ucfirst($column) !!}</th>
                        @endforeach
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cars['data'] as $car)
                        <tr>
                            @foreach($car as $data)
                                <td>{!! ucfirst($data) !!}</td>
                            @endforeach
                            <td colspan="3">
                                {!! Form::open(['route' => ['car.destroy', $car['id']], 'method' => 'DELETE', 'class' => 'form-delete']) !!}
                                    <div class="btn-group">
                                        <a href="{!! route('car.show', [$car['id']]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
                                        <a href="{!! route('car.edit', [$car['id']]) !!}" class='btn btn-info btn-xs'><i class="fa fa-edit"></i></a>
                                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                    </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $results->links() !!}
        </div>
    </div>
@stop

@section('js')

@stop