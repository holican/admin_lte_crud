@extends('admin.home')

@section('css')

@stop

@section('content_header')
    <div class="text-center">
        <label for="car">Create Car</label>
    </div>
@stop

@section('content')
    @include('layouts.errors')
    <div class="panel-body">
        {!! Form::open(['route' => 'car.store', 'method' => 'post']) !!}
            @foreach($columns as $key=>$column)
                <div class="row">
                    <div class="form-group col-sm-6 {{ $errors->has($column) ? 'has-error' : '' }}">
                        <label for="{!! $column !!}">{!! ucfirst($column) !!}</label>
                        @if($types[$key] == 'blob')
                            <input type="file" name="{!! $column !!}">
                        @elseif($types[$key] == 'text' || $types[$key] == 'longtext')
                            <textarea name="{!! $column !!}" cols="30" rows="10" class="form-control"></textarea>
                        @elseif($types[$key] == 'string' || $types[$key] == 'integer' || $types[$key] == 'decimal')
                            <input type="text" name="{!! $column !!}" class="form-control">
                        @elseif($type[$key] == 'date' || $types[$key] == 'datetime')
                            <input type="date" name="{!! $column !!}" class="form-control">
                        @endif
                        @if($errors->has( $column ))
                            <span class="help-block">{{ $errors->first( $column ) }}</span>
                        @endif
                    </div>
                </div>
            @endforeach
            <div class="form-group col-sm-12">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('car.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@stop

@section('js')

@stop