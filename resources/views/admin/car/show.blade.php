@extends('admin.home')

@section('css')

@stop

@section('content_header')
    <div class="text-center">
        <label for="Car">Car</label>
    </div>
@stop

@section('content')
<div class="panel-body">
    <div class="col-sm-6">
        <table class="table table-striped table-bordered">
            @foreach($columns as $column)
                <tr>
                    <th><label for="{!! $column !!}">{!! ucfirst($column) !!}</label></th>
                    <td>{!! $car->$column !!}</td>
                </tr>
            @endforeach
        </table>
        <a href="{!! route('car.index') !!}" class="btn btn-primary">Back</a>
    </div>
</div>
@stop

@section('js')

@stop