@extends('admin.home')

@section('css')

@stop

@section('content_header')
    <div class="text-center">
        <label for="car">Update Car</label>
    </div>
@stop

@section('content')
    @include('layouts.errors')
    <div class="panel-body">
        {!! Form::open(['route' => ['car.update', $car->id], 'method' => 'put']) !!}
             @foreach($columns as $key=>$column)
                <div class="row">
                    <div class="form-group col-sm-6 {{ $errors->has($column) ? 'has-error' : '' }}">
                        <label for="{!! $column !!}">{!! ucfirst($column) !!}</label>
                        @if($types[$key] == 'string')
                            <input type="text" name="{!! $column !!}" class="form-control" value="{!! $car->$column !!}">
                        @elseif($types[$key] == 'integer')
                            <input type="text" name="{!! $column !!}" class="form-control" value="{!! $car->$column !!}">
                        @else
                            <textarea name="{!! $column !!}" cols="30" rows="10" class="form-control">{!! $car->$column !!}</textarea>
                        @endif
                        @if($errors->has( $column ))
                            <span class="help-block">{{ $errors->first( $column ) }}</span>
                        @endif
                    </div>
                </div>
            @endforeach
            <div class="form-group col-sm-12">
                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('car.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@stop

@section('js')

@stop