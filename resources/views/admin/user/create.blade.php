@extends('admin.home')

@section('content_header')
    <div class="text-center">
        <label for="admin-users">Create Admin User</label>
    </div>
@stop

@section('content')
    @include('layouts.errors')
    <div class="panel-body">
        {!! Form::open(['route' =>'admins.store', 'method' => 'post']) !!}
            <div class="form-group col-sm-3 {{ $errors->has('name') ? 'has-error' : '' }}">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                @if($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
            </div>
            <div class="form-group col-sm-3">
                {!! Form::label('role_id', 'Role') !!}
                {!! Form::select('role_id', $roles, null, ['class' => 'form-control','id' => 'role_id']) !!}
            </div>
            <div class="form-group col-sm-3 {{ $errors->has('email') ? 'has-error' : '' }}">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                @if($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <div class="form-group col-sm-3 {{ $errors->has('password') ? 'has-error' : '' }}">
                {!! Form::label('password', 'Password') !!}
                {!! Form::text('password', null, ['class' => 'form-control']) !!}
                @if($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>
            <div class="form-group col-sm-12">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('admins.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@stop