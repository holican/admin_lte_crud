@extends('admin.home')

@section('content_header')
    <div class="text-center">
        <label for="admin-users">Update Admin User</label>
    </div>
@stop

@section('content')
    @include('layouts.errors')
    <div class="panel-body">
        {!! Form::open(['route' => ['admins.update', $user->id], 'method' => 'put']) !!}
            <div class="form-group col-sm-3">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-3">
                {!! Form::label('role_id', 'Role') !!}
                {!! Form::select('role_id', $roles, $user->getRoleNames(), ['class' => 'form-control','id' => 'role_id']) !!}
            </div>
            <div class="form-group col-sm-3">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', $user->email, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-3">
                {!! Form::label('password', 'Password') !!}
                {!! Form::text('password', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-12">
                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('admins.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@stop