@extends('admin.home')

@section('content_header')
    <ol class="breadcrumb pull-right">
        <li class="breadcrumb-item"><a class="btn btn-primary" href="{!! route('admins.create') !!}" style="color:white">Add New</a></li>
    </ol>
    <br><br>
@stop

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block" style="margin-top: 3%;">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('danger'))
        <div class="alert alert-danger alert-block" style="margin-top: 3%;">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="panel panel-inverse" data-sortable-id="index-1">
        <div class="panel-heading">
            <label class="panel-title">&nbsp;Admins</label>
        </div>
        <div class="panel-body">
            <table class="table display table-striped table-bordered dt-responsive" >
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{!! $user->name !!}</td>
                            <td>
                                @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $role)
                                        @if($role == 'root-admin')
                                            <label class="badge btn-success">{!! $role !!}</label>
                                        @elseif ($role == 'admin')
                                            <label class="badge btn-primary">{!! $role !!}</label>
                                        @else 
                                            <label class="badge">{!! $role !!}</label>
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <td>{!! $user->email !!}</td>
                            <td>
                                {!! Form::open(['route' => ['admins.destroy', $user->id], 'method' => 'DELETE', 'class' => 'form-delete']) !!}
                                    <div class="btn-group">
                                        <a href="{!! route('admins.show', [$user->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
                                        <a href="{!! route('admins.edit', [$user->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-edit"></i></a>
                                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                    </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="text-center">
        {!! $users->links() !!}
    </div>
@stop