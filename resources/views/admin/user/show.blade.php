@extends('admin.home')

@section('content_header')

@stop

@section('content')
<div class="panel-body">
    <div class="col-sm-6">
        <table class="table table-striped table-bordered">
            <!-- Id Field -->
            <tr>
                <th>{!! Form::label('id', 'Id:') !!}</th>
                <td>{!! $user->id !!}</td>
            </tr>
            <!-- Name Field -->
            <tr>
                <th>{!! Form::label('name', 'Name:') !!}</th>
                <td>{!! $user->name !!}</td>
            </tr>
            <tr>
                <th>{!! Form::label('role_id', 'Role:') !!}</th>
                @foreach($user->getRoleNames() as $role)
                    <td><label class="badge">{!! $role !!}</label></td>
                @endforeach
            </tr>
            <tr>
                <th>{!! Form::label('email', 'Email') !!}</th>
                <td>{!! $user->email !!}</td>
            </tr>
        </table>
        <a href="{!! route('admins.index') !!}" class="btn btn-primary">Back</a>
    </div>
</div>
@stop