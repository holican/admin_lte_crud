@extends('admin.home')

@section('content_header')
    <ol class="breadcrumb pull-right">
        <li class="breadcrumb-item"><a class="btn btn-primary" href="{!! route('tables.create', $table) !!}" style="color:white">Add New Column</a></li>
    </ol>
    <br><br>
@stop

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block" style="margin-top: 3%;">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @elseif ($message = Session::get('danger'))
        <div class="alert alert-danger alert-block" style="margin-top: 3%;">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    <label for="roles">Table Name: &nbsp;{!! ucfirst($table)!!}&nbsp;Table Column Lists</label>
    <table class="table display table-striped table-bordered dt-responsive" >
        <thead>
            <tr>
                <th>Column Name</th>
                <th>Types</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($columns as $key=>$column)
                <tr>
                    <td>{!! $column !!}</td>
                    <td>{!! $types[$key] !!}</td>
                    <td>
                        {!! Form::open(['route' => ['tables.destroy', $table, $column], 'method' => 'post', 'class' => 'form-delete']) !!}
                            <div class="btn-group">
                                <a href="{!! route('tables.show',[$column, $types[$key]]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
                                <a href="{!! route('tables.edit', [$table, $column]) !!}" class='btn btn-info btn-xs'><i class="fa fa-edit"></i></a>
                                {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop

