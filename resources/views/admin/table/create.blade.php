@extends('admin.home')

@section('css')

@stop

@section('content_header')
    <div class="text-center">
    <label for="car">Create {{ ucfirst($table) }}'s Fields</label>
    </div>
@stop

@section('content')
    @include('layouts.errors')
    <div class="panel-body">
        {!! Form::open(['route' => 'tables.store', 'method' => 'post']) !!}
            <input type="hidden" name="table" value="{!! $table !!}">
            <div class="form-group col-sm-6">
                {!! Form::label('name', 'Name: ') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-6">
                {!! Form::label('type', 'Column Type: ') !!}
                <select name="type" id="" class="form-control">
                    <option value="string">String (For Text, E.g: name. email, image, etc...)</option>
                    <option value="text">Text (For Description)</option>
                    <option value="longText">Long Text (For Paragraph)</option>
                    <option value="unsignedInteger">Integer (For Number E.g: 1, 2 , 3, 4, 5)</option>
                    <option value="decimal">Decimal (For Decimal E.g: 7.5 7.33)</option>
                    <option value="dateTime">DateTime (For Date and Time)</option>
                    <option value="date">Date (For Date Only E.g: Date of Birth)</option>
                    <option value="binary">Image</option>
                </select>
            </div>
            <br>
            <div class="col-sm-12" id="field_input"></div>
            <br>
            <div class="form-group col-sm-12">
                {!! Form::submit('Add Column', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route($view.'.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@stop

@section('js')

@stop