@extends('admin.home')

@section('css')

@stop

@section('content_header')
    <div class="text-center">
        <label for="car">Rename Column</label>
    </div>
@stop

@section('content')
    @include('layouts.errors')
    <div class="panel-body">
        {!! Form::open(['route' => ['tables.updated', $column], 'method' => 'post']) !!}
            <input type="hidden" name="table" value="{!! $table !!}">
            <div class="form-group col-sm-6">
                {!! Form::label('name', 'Name: ') !!}
                {!! Form::text('name', $column, ['class' => 'form-control', 'readonly']) !!}
            </div>
            <div class="form-group col-sm-6">
                {!! Form::label('name', 'Rename Column: ') !!}
                {!! Form::text('rename', null, ['class' => 'form-control']) !!}
            </div>
            <br>
            <div class="col-sm-12" id="field_input"></div>
            <br>
            <div class="form-group col-sm-12">
                {!! Form::submit('Rename Column', ['class' => 'btn btn-primary']) !!}
            <a onclick="goBack()" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@stop

@section('js')
<script>
    function goBack() {
      window.history.back();
    }
</script>
@stop