@extends('admin.home')

@section('content_header')

@stop

@section('content')
<div class="panel-body">
    <div class="col-sm-6">
        <table class="table table-striped table-bordered">
            <tr>
                <th><label for="{!! $column !!}">Name :</label></th>
                <td>{!! $column  !!}</label></td>
            </tr>
            <tr>
                <th><label for="{!! $type !!}">Type :</label></th>
                <td>{!! $type !!}</td>
            </tr>
        </table>
        <a onclick="goBack()" class="btn btn-primary">Back</a>
    </div>
</div>
@stop

@section('js')
<script>
    function goBack() {
        window.history.back();
    }
</script>
@stop