@extends('admin.home')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/css/fontawesome-iconpicker.css" integrity="sha256-KC2XUWlJX7EuWPF02ZakG9K7ny3Y9AYDfmdnQD/pLlU=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/css/fontawesome-iconpicker.min.css" integrity="sha256-yExEWA6b/bqs3FXxQy03aOWIFtx9QEVnHZ/EwemRLbc=" crossorigin="anonymous" />
@stop

@section('content_header')
    <div class="text-center">
        <label for="menu">Create Menu</label>
    </div>
@stop

@section('content')
    @include('layouts.errors')
    <div class="panel-body">
        {!! Form::open(['route' => ['menus.update', $menu->id], 'method' => 'put']) !!}
            <div class="form-group col-sm-4">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', $menu->name, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-4">
                {!! Form::label('icon', 'Icon') !!}
                <div class="input-group">
                <input data-placement="bottomRight" class="form-control icon" value="{{ $menu->icon }}"
                    type="text" name="icon" />
                    <span class="input-group-addon"></span>
                </div>
            </div>
            <div class="form-group col-sm-4">
                {!! Form::label('order_by', 'Order By') !!}
                {!! Form::text('order_by', $menu->order_by, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-12">
                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('menus.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::close() !!}
    </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/js/fontawesome-iconpicker.js" integrity="sha256-sAlwRjlYvlekTLgTnPQK/oO1b+zc2Phz6cbLRVXWfyI=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/js/fontawesome-iconpicker.min.js" integrity="sha256-QvBM3HnWsvcCWqpqHZrtoefjB8t2qPNzdSDNyaSNH5Q=" crossorigin="anonymous"></script>
    <script>
        $('.icon').iconpicker();
    </script>
@stop