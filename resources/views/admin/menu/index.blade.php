@extends('admin.home')

@section('css')

@stop

@section('content_header')
    <ol class="breadcrumb pull-right">
        <li class="breadcrumb-item"><a class="btn btn-primary" href="{!! route('menus.create') !!}" style="color:white">Add New</a></li>
    </ol>
    <br><br>
@stop

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block" style="margin-top: 3%;">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('danger'))
        <div class="alert alert-danger alert-block" style="margin-top: 3%;">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="panel panel-inverse" data-sortable-id="index-1">
        <div class="panel-heading">
            <label class="panel-title">&nbsp;Menus</label>
        </div>
        <div class="panel-body">
            <table class="table display table-striped table-bordered dt-responsive" >
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Icon</th>
                        <th>Icon Type</th>
                        <th>Side Bar</th>
                        <th>Order By</th>
                        <th>Actions</th>
                        <th>Add To Sidebar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($menus as $menu)
                        <tr>
                            <td>{!! $menu->id !!}</td>
                            <td>{!! $menu->name !!}</td>
                            <td><i class="{!! $menu->icon !!}"></i></td>
                            <td>{!! $menu->icon !!}</td>
                            <td>
                                @if($menu->sidebar != '0')
                                    <label class="badge btn-success">True</label>
                                @else 
                                    <label class="badge btn-primary">False</label>
                                @endif
                            </td>
                            <td>{!! $menu->order_by !!}</td>
                            <td>
                                {!! Form::open(['route' => ['menus.destroy', $menu->id], 'method' => 'DELETE', 'class' => 'form-delete', 'id' => 'form']) !!}
                                    <div class="btn-group">
                                        <a href="{!! route('menus.show', $menu->id) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
                                        @if(Auth::user()->hasRole('root-admin'))
                                            <a href="{!! route('menus.edit', $menu->id) !!}" class='btn btn-info btn-xs'><i class="fa fa-edit"></i></a>
                                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure DELETE?')"]) !!}
                                        @endif
                                    </div>
                                {!! Form::close() !!}
                            </td>
                            <td>
                                @if($menu->sidebar == '0')
                                    {!! Form::open(['route' => ['menus.sidebar', $menu->id], 'method' => 'POST']) !!}
                                        {!! Form::button('<i class="fa fa-sliders-h"></i>', ['type' => 'submit', 'class' => 'btn btn-primary btn-xs', 'onclick' => "return confirm('Add menu to Sidebar?')"]) !!}
                                    {!! Form::close() !!}
                                @else 
                                    {!! Form::open(['route' => ['menus.removesidebar', $menu->id], 'method' => 'POST']) !!}
                                        {!! Form::button('<i class="fa fa-ban"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure REMOVE  menu to Sidebar?')"]) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="text-center">
        {!! $menus->links() !!}
    </div>
@stop