@extends('admin.home')

@section('content_header')

@stop

@section('content')
<div class="panel-body">
    <div class="col-sm-6">
        <table class="table table-striped table-bordered">
            <!-- Id Field -->
            <tr>
                <th>{!! Form::label('id', 'Id:') !!}</th>
                <td>{!! $menu->id !!}</td>
            </tr>
            <!-- Name Field -->
            <tr>
                <th>{!! Form::label('name', 'Name:') !!}</th>
                <td>{!! $menu->name !!}</td>
            </tr>
            <tr>
                <th>{!! Form::label('icon', 'Icon:') !!}</th>
                <td><i class="{!! $menu->icon !!}"></i></td>
            </tr>
            <tr>
                <th>{!! Form::label('icon name', 'Icon Name:') !!}</th>
                <td>{!! $menu->icon !!}</td>
            </tr>
            <tr>
                <th>{!! Form::label('order_by', 'Order By') !!}</th>
                <td>{!! $menu->order_by !!}</td>
            </tr>
        </table>
        <a href="{!! route('menus.index') !!}" class="btn btn-primary">Back</a>
    </div>
</div>
@stop