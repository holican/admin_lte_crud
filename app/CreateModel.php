<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateModel extends Model
{
    public static function createDashboard($name)
    {
        return Artisan::call('create:dashboard', ['name' => $name]);
    }

    public static function removeDashboard($name)
    {
        return Artisan::call('remove:dashboard', ['name' => $name]);
    }

    public static function addColumnToTable($table_name, $fields)
    {
        if (Schema::hasTable($table_name)) {
            Schema::table($table_name, function (Blueprint $table) use ($fields, $table_name) {
                foreach ($fields as $field) {
                    $table->{$field['type']}($field['name'])->nullable();
                }
            });
        }
    }
    
    public static function getColumnLists($table)
    {
        $skip = ['created_at', 'updated_at', 'deleted_at'];
        
        $lists = Schema::getColumnListing($table);
        foreach ($lists as $list) {
            if (! in_array ( $list, $skip )) {
                $columns[] = $list;
            }
        }
        return $columns;
    }

    public static function getColumnTypes($table, $columns)
    {
        foreach ($columns as $column) {
            $types[] = Schema::getColumnType($table, $column);
        }
        return $types;
    }

    public static function dropColumn($table_name, $column)
    {
        if (Schema::hasTable($table_name)) {
            Schema::table($table_name, function (Blueprint $table) use ($column, $table_name) {
                $table->dropColumn($column);
            });
        }
    }

    public static function renameColumn($table_name, $column, $rename)
    {
        if (Schema::hasTable($table_name)) {
            Schema::table($table_name, function (Blueprint $table) use ($column, $rename, $table_name) {
                $table->renameColumn($column, $rename);
            });
        }
    }
}