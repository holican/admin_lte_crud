<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;

class RemoveFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:dashboard
    {name : Class (singular) for example User}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Admin Dashboard Folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = ucfirst($this->argument('name'));
        $this->removeFolder($name);
        $this->removeController($name);
        $this->removeModel($name);
        $this->removeRequest($name);
        $this->removeRoute($name);
        $this->migrateRollBack($name);
        $this->dropDatabase($name);
    }
    private function removeModel($name)
    {
        $model_path = app_path("/Models/{$name}.php");
        if (file_exists($model_path)) {
            unlink($model_path) or die("File not exists");
        }
    }
    private function removeController($name)
    {
        $controller_path = app_path("/Http/Controllers/Admin/{$name}Controller.php");
        if (file_exists($controller_path)) {
            unlink($controller_path) or  die("File not exists");
        }
    }
    private function removeRequest($name)
    {
        $request_path = app_path("/Http/Requests/{$name}Request.php");
        if (file_exists($request_path)) {
            unlink($request_path) or die("File not exists");
        }
    }
    private function removeFolder($name)
    {
        $lowerCase = strtolower($name);
        $folder_path = resource_path("views/admin/$lowerCase");
        if (file_exists($folder_path)) {
            unlink("$folder_path/create.blade.php");
            unlink("$folder_path/edit.blade.php");
            unlink("$folder_path/index.blade.php");
            unlink("$folder_path/show.blade.php");
            rmdir($folder_path);
        }
    }
    protected function getData($name)
    {
        return file_get_contents(resource_path("views/CreateFolder/{$name}.stub"));
    }
    private function removeRoute($name)
    {
        $route_path = base_path("routes/web.php");
        $routesContents = file_get_contents($route_path);
        $routeTemplate = str_replace(
            [
                '{{routeNameSingularLower}}',
                '{{routeName}}'
            ],
            [
                strtolower($name),
                $name
            ],
            $this->getData('Route')
        );
        $routeReplace = str_replace($routeTemplate, '', $routesContents);
        file_put_contents($route_path, $routeReplace);
    }
    private function migrateRollBack($name)
    {
        $modelName = strtolower($name);
        $path = base_path('database/migrations');
        $allFiles = File::allFiles($path);
        $fileName = "create_".$modelName."s_table.php";
        foreach ($allFiles as $key => $file) {
            $files[] = $file->getFileName();
        }
        $files = array_reverse($files);
        foreach ($files as $file) {
            if (Str::contains($file, $fileName)) {
                $migrateFile = $file;
                unlink("$path/$migrateFile");
            }
        }
    }
    private function dropDatabase($name)
    {
        $tableName = strtolower($name)."s";
        Schema::dropIfExists($tableName);
    }
}