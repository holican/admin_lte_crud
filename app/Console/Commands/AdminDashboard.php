<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Artisan;

class AdminDashboard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:dashboard
    {name : Class (singular) for example User}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create New Admin Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = ucfirst($this->argument('name'));
        $this->createFolder($name);
        $this->createViews($name);
        $this->makeModel($name);
        $this->makeController($name);
        $this->makeRequest($name);
        $this->makeMigrate($name);
        $this->addRoute($name);
        $this->runMigrate();
    }

    protected function createFolder($name)
    {
        $models = base_path('app/Models');
        if (!file_exists($models)) {
            mkdir(base_path('app/Models'), 0777, true);
        }
        $adminController = app_path('/Http/Controllers/Admin');
        if (!file_exists($adminController)) {
            mkdir(app_path('/Http/Controllers/Admin'), 0777, true);
        }
        $request = app_path('/Http/Requests');
        if (!file_exists($request)) {
            mkdir(app_path('/Http/Requests'), 0777, true);
        }
        $admin_view = resource_path('views/admin');
        if (!file_exists($admin_view)) {
            mkdir(resource_path('views/admin'), 0777, true);
        }
    }
    protected function createViews($name)
    {
        $lowerCase = strtolower($name);
        mkdir(resource_path("views/admin/{$lowerCase}"), 0777, true);
        $this->createBlade($name);
    }
    protected function getData($name)
    {
        return file_get_contents(resource_path("views/CreateFolder/{$name}.stub"));
    }
    protected function makeModel($name)
    {
        $tableName = strtolower($name)."s";
        $modelTemplate = str_replace(
            [
                '{{modelName}}',
                '{{tableName}}'
            ],
            [
                $name,
                $tableName
            ],
            $this->getData('Model')
        );
        file_put_contents(app_path("/Models/{$name}.php"), $modelTemplate);
    }
    protected function makeController($name)
    {
        $controllerTemplate = str_replace(
            [
                '{{modelName}}',
                '{{modelNamePluralLowerCase}}',
                '{{modelNameSingularLowerCase}}'
            ],
            [
                $name,
                strtolower($name),
                strtolower($name)
            ],
            $this->getData('Controller')
        );
        file_put_contents(app_path("/Http/Controllers/Admin/{$name}Controller.php"), $controllerTemplate);
    }
    protected function makeRequest($name)
    {
        $requestTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getData('Request')
        );

        file_put_contents(app_path("/Http/Requests/{$name}Request.php"), $requestTemplate);
    }
    protected function getBlade($name, $bladeName)
    {
        $blade = str_replace(
            [
                '{{modelName}}',
                '{{modelNameSingularLowerCase}}'
            ],
            [
                $name,
                strtolower($name)
            ],
            $this->getData($bladeName)
        );
        return $blade;
    }
    protected function createBlade($name)
    {
        $lowerCase = strtolower($name);
        $blade_path = resource_path("views/admin/$lowerCase");
        $index = file_put_contents("$blade_path/index.blade.php", $this->getBlade($name, 'Index'));
        $create = file_put_contents("$blade_path/create.blade.php", $this->getBlade($name, 'Create'));
        $edit = file_put_contents("$blade_path/edit.blade.php", $this->getBlade($name, 'Edit'));
        $show = file_put_contents("$blade_path/show.blade.php", $this->getBlade($name, 'Show'));
    }
    protected function makeMigrate($name)
    {
        $migrateName = "create_".strtolower($name)."s_table.php";
        $date = date('Y_m_d_His_');
        $fileName = $date.$migrateName;
        $migrateTemplate = str_replace(
            [
                '{{migrateName}}',
                '{{migrateTableName}}'
            ],
            [
                $name,
                strtolower($name)
            ],
            $this->getData('Migrate')
        );
        file_put_contents(base_path("database/migrations/$fileName"), $migrateTemplate);
    }
    protected function addRoute($name)
    {
        $route_file = base_path("routes/web.php");
        $routeContents = file_get_contents($route_file);
        $routeTemplate = str_replace(
            [
                '{{routeNameSingularLower}}',
                '{{routeName}}'
            ],
            [
                strtolower($name),
                $name
            ],
            $this->getData('Route')
        );
        $routes = $routeContents."\r\n".$routeTemplate;
        if (file_exists($route_file)) {
            $fp = fopen($route_file, "w+");
            fwrite($fp, $routes);
        }
        fclose($fp);
    }
    protected function runMigrate()
    {
        return Artisan::call('migrate');
    }
}