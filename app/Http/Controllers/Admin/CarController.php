<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CarRequest;
use Illuminate\Http\Request;
use App\Models\Car;
use App\CreateModel;

class CarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $table = 'cars';
        $columns = CreateModel::getColumnLists($table);
        $results = Car::select($columns)->paginate(10);
        $cars = $results->toArray();

        return view('admin.car.index', compact('cars', 'columns', 'table', 'results'));
    }

    public function create()
    {
        $columns = CreateModel::getColumnLists('cars');
        $types = CreateModel::getColumnTypes('cars', $columns);
        $columns = array_slice($columns, 1);
        $types = array_slice($types, 1);
        return view('admin.car.create', compact('columns', 'types'));
    }

    public function store(CarRequest $request)
    {
        $car = Car::create($request->all());

        return redirect(route('car.index'))
            ->with('success', 'Car saved successfully.');
    }

    public function show($id)
    {
        $car = Car::findOrFail($id);
        $columns = CreateModel::getColumnLists('cars');

        return view('admin.car.show', compact('car', 'columns'));
    }

    public function edit($id)
    {
        $car = Car::findOrFail($id);
        $columns = CreateModel::getColumnLists('cars');
        $types = CreateModel::getColumnTypes('cars', $columns);
        $columns = array_slice($columns, 1);
        $types = array_slice($types, 1);

        return view('admin.car.edit', compact('car', 'columns', 'types'));
    }

    public function update(CarRequest $request, $id)
    {
        $car = Car::findOrFail($id);
        $car->update($request->all());

        return redirect(route('car.index'))
            ->with('success', 'Car updated successfully.');
    }

    public function destroy($id)
    {
        Car::destroy($id);

        return redirect()->back()
            ->with('danger', 'Car deleted successfully');
    }
}