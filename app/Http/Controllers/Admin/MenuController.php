<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MenuRequest;
use Illuminate\Http\Request;
use App\Models\Menu;
use App\CreateModel;
use Auth;

class MenuController extends Controller
{
    public function index()
    {
        if (Auth::user()->hasRole('root-admin')) {
            $menus = Menu::whereNull('deleted_at')->paginate(10);
        }else{
            $menus = Menu::whereNotIn('id', ['1', '2'])->whereNull('deleted_at')->paginate(10);
        }
        
        return view('admin.menu.index', compact('menus'));
    }

    public function create()
    {
        return view('admin.menu.create');
    }

    public function store(MenuRequest $request)
    {
        $menu = Menu::create($request->all());
        // $createMenu = CreateModel::createDashboard($request->name);
        
        return redirect(route('menus.index'))
            ->with('success', 'Menu saved successfully.');
    }

    public function show($id)
    {
        $menu = Menu::findOrFail($id);

        return view('admin.menu.show', compact('menu'));
    }

    public function edit($id)
    {
        $menu = Menu::findOrFail($id);

        return view('admin.menu.edit', compact('menu'));
    }

    public function update(MenuRequest $request, $id)
    {
        $menu = Menu::findOrFail($id);
        $menu->update($request->all());

        return redirect(route('menus.index'))
            ->with('success', 'Menu updated successfully.');
    }

    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);
        $remove_menu = CreateModel::removeDashboard($menu->name);
        Menu::destroy($id);
        
        return redirect()->back()
            ->with('danger', 'Menu deleted successfully');
    }

    public function addSidebar($id)
    {
        $menu = Menu::findOrFail($id);
        $model = base_path('app/Models/').ucfirst($menu->name).".php";
        if (!file_exists($model)) {
            $createMenu = CreateModel::createDashboard($menu->name);
        }
        Menu::where('id', $menu->id)->update([
            'sidebar' => '1'
        ]);
        return redirect()->back();
    }

    public function removeSidebar($id)
    {
        $menu = Menu::where('id', $id)->update([
            'sidebar' => '0'
        ]);
        return redirect()->back();
    }
}