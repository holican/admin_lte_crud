<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CreateModel;

class TableController extends Controller
{
    
    public function index($table)
    {
        $columns = CreateModel::getColumnLists($table);
        $types = CreateModel::getColumnTypes($table, $columns);
        $columns = array_slice($columns, 1);
        $types = array_slice($types, 1);
        
        return view('admin.table.index', compact('columns', 'types', 'table'));
    }

    public function create($table)
    {
        $view = substr($table, 0, -1);
        return view('admin.table.create', compact('table', 'view'));
    }

    public function store(Request $request)
    {
        $table = $request->table;
        $fields = [
            ['name' => strtolower($request->name), 'type' => strtolower($request->type)]
        ];
        CreateModel::addColumnToTable($table, $fields);
        $view = substr($table, 0, -1);
        
        return redirect(route($view.'.index'));
    }

    public function show($column, $type)
    {
        return view('admin.table.show', compact('column', 'type'));
    }

    public function edit($table, $column)
    {
        return view('admin.table.edit', compact('column', 'table'));
    }

    public function update(Request $request, $column)
    {
        $rename = strtolower($request->rename);
        $table = $request->table;
        CreateModel::renameColumn($table, $column, $rename);
        $view = substr($table, 0, -1);
        
        return redirect(route($view.'.index'));
    }

    public function destroy($table, $column)
    {
        CreateModel::dropColumn($table, $column);
        $view = substr($table, 0, -1);
         return redirect()->back()
         ->with('danger', 'Column deleted successfully');
    }
}