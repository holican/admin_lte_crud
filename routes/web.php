<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', 'Auth\LoginController@showLoginForm');
Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('admin/logout', 'Auth\LoginController@logout')->name('logout');

Route::prefix('admin')->group(function () {
    Route::group(['middleware' => ['auth', 'role:root-admin']], function () {
        Route::resource('admins', 'Admin\AdminController');
        Route::resource('roles', 'Admin\RoleController');
        Route::resource('menus', 'Admin\MenuController');
    });
    Route::group(['middleware' => ['auth', 'role:root-admin|admin']], function () {
        Route::resource('menus', 'Admin\MenuController')->except('destroy', 'edit', 'update');
    });
    Route::get('/', 'Admin\HomeController@home')->name('admin-home');
    Route::post('/menus/addsidebar/{id}', 'Admin\MenuController@addSidebar')->name('menus.sidebar');
    Route::post('/menus/removesidebar/{id}', 'Admin\MenuController@removeSidebar')->name('menus.removesidebar');
    Route::post('/menus/addcolumn', 'Admin\MenuController@addFields')->name('menus.addcolumn');
    Route::get('tables/{table}', 'Admin\TableController@index')->name('tables.index');
    Route::get('tables/create/{table}', 'Admin\TableController@create')->name('tables.create');
    Route::post('tables/store', 'Admin\TableController@store')->name('tables.store');
    Route::get('tables/show/{column}/{type}', 'Admin\TableController@show')->name('tables.show');
    Route::get('tables/{table}/{column}', 'Admin\TableController@edit')->name('tables.edit');
    Route::post('tables/update/{column}', 'Admin\TableController@update')->name('tables.updated');
    Route::post('tables/delete/{table}/{column}', 'Admin\TableController@destroy')->name('tables.destroy');
});

Route::prefix('admin')->group(function () {
    Route::resource('car', 'Admin\CarController');
});