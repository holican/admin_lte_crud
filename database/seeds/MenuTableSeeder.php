<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Menu::create([
           'name' => 'Admins',
           'icon' => 'fas fa-user',
           'sidebar' => '1',
           'order_by' => '1'
        ]);
        $role = Menu::create([
            'name' => 'Roles',
            'icon' => 'fas fa-tasks',
            'sidebar' => '1',
            'order_by' => '2'
        ]);
        $menu = Menu::create([
            'name' => "Menus",
            'icon' => "fas fa-align-justify",
            'sidebar' => '1',
            'order_by' => '3'
        ]);
    }
}