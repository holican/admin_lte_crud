<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create Role
        $rootAdmin = Role::create(['name' => 'root-admin']);
        $admin = Role::create(['name' => 'admin']);
        $operator = Role::create(['name' => 'operator']);
    }
}