<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rootAdmin = User::create([
            'role_id' => 1,
            'name' => "RootAdmin",
            'email' => "root@admin.com",
            'password' => bcrypt('password')
        ]);
        $rootAdmin->assignRole('root-admin');
        
        $admin = User::create([
            'role_id' => 2,
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('adminpassword')
        ]);
        $admin->assignRole('admin');
    }
}